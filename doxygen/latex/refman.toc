\contentsline {chapter}{\numberline {1}\discretionary {-}{}{}File \discretionary {-}{}{}Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\discretionary {-}{}{}File \discretionary {-}{}{}List}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}\discretionary {-}{}{}File \discretionary {-}{}{}Documentation}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}4011-\/\discretionary {-}{}{}Tick\discretionary {-}{}{}Tock.c \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{4}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{4}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}F\discretionary {-}{}{}O\discretionary {-}{}{}S\discretionary {-}{}{}C}{4}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}main}{4}{subsubsection.2.1.2.2}
\contentsline {subsubsection}{\numberline {2.1.2.3}show\discretionary {-}{}{}Pos}{5}{subsubsection.2.1.2.3}
\contentsline {subsection}{\numberline {2.1.3}\discretionary {-}{}{}Variable \discretionary {-}{}{}Documentation}{5}{subsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.3.1}l\discretionary {-}{}{}Time}{5}{subsubsection.2.1.3.1}
\contentsline {subsubsection}{\numberline {2.1.3.2}n\discretionary {-}{}{}Action}{5}{subsubsection.2.1.3.2}
\contentsline {subsubsection}{\numberline {2.1.3.3}n\discretionary {-}{}{}Dirty}{6}{subsubsection.2.1.3.3}
\contentsline {subsubsection}{\numberline {2.1.3.4}n\discretionary {-}{}{}Pos}{6}{subsubsection.2.1.3.4}
\contentsline {subsubsection}{\numberline {2.1.3.5}n\discretionary {-}{}{}Ticker}{6}{subsubsection.2.1.3.5}
\contentsline {section}{\numberline {2.2}\discretionary {-}{}{}T2interrupt.c \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{7}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.1}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}\_\discretionary {-}{}{}attribute\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}\_\discretionary {-}{}{}}{7}{subsubsection.2.2.2.1}
\contentsline {subsection}{\numberline {2.2.3}\discretionary {-}{}{}Variable \discretionary {-}{}{}Documentation}{7}{subsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.3.1}l\discretionary {-}{}{}Time}{7}{subsubsection.2.2.3.1}
\contentsline {subsubsection}{\numberline {2.2.3.2}n\discretionary {-}{}{}Dirty}{7}{subsubsection.2.2.3.2}
\contentsline {subsubsection}{\numberline {2.2.3.3}n\discretionary {-}{}{}Ticker}{7}{subsubsection.2.2.3.3}
\contentsline {section}{\numberline {2.3}\discretionary {-}{}{}T3interrupt.c \discretionary {-}{}{}File \discretionary {-}{}{}Reference}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}\discretionary {-}{}{}Detailed \discretionary {-}{}{}Description}{9}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}\discretionary {-}{}{}Define \discretionary {-}{}{}Documentation}{9}{subsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.2.1}\discretionary {-}{}{}P\discretionary {-}{}{}B\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}D\discretionary {-}{}{}O\discretionary {-}{}{}W\discretionary {-}{}{}N}{9}{subsubsection.2.3.2.1}
\contentsline {subsubsection}{\numberline {2.3.2.2}\discretionary {-}{}{}P\discretionary {-}{}{}B\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}L\discretionary {-}{}{}E\discretionary {-}{}{}F\discretionary {-}{}{}T}{9}{subsubsection.2.3.2.2}
\contentsline {subsubsection}{\numberline {2.3.2.3}\discretionary {-}{}{}P\discretionary {-}{}{}B\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}R\discretionary {-}{}{}I\discretionary {-}{}{}G\discretionary {-}{}{}H\discretionary {-}{}{}T}{9}{subsubsection.2.3.2.3}
\contentsline {subsubsection}{\numberline {2.3.2.4}\discretionary {-}{}{}P\discretionary {-}{}{}B\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}U\discretionary {-}{}{}P}{9}{subsubsection.2.3.2.4}
\contentsline {subsection}{\numberline {2.3.3}\discretionary {-}{}{}Function \discretionary {-}{}{}Documentation}{9}{subsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.3.1}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}\_\discretionary {-}{}{}attribute\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}\_\discretionary {-}{}{}}{9}{subsubsection.2.3.3.1}
\contentsline {subsection}{\numberline {2.3.4}\discretionary {-}{}{}Variable \discretionary {-}{}{}Documentation}{9}{subsection.2.3.4}
\contentsline {subsubsection}{\numberline {2.3.4.1}n\discretionary {-}{}{}Action}{10}{subsubsection.2.3.4.1}
\contentsline {subsubsection}{\numberline {2.3.4.2}n\discretionary {-}{}{}Buttons}{10}{subsubsection.2.3.4.2}
\contentsline {subsubsection}{\numberline {2.3.4.3}n\discretionary {-}{}{}Off\discretionary {-}{}{}Count}{10}{subsubsection.2.3.4.3}
\contentsline {subsubsection}{\numberline {2.3.4.4}n\discretionary {-}{}{}On\discretionary {-}{}{}Count}{10}{subsubsection.2.3.4.4}
