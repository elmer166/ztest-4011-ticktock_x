/*! \file T2interrupt.c
 *
 *  \brief Timer 2 ISR for 4011-TickTock
 *
 *  Maintains the time for 4011-TickTock
 *
 */
#include <p30Fxxxx.h>

//! Current time (actually, a time_t)
extern long lTime;
//! Used by ISR to stretch out the timer ticks
extern int nTicker;
//! Does the display ned to be updated?
extern int nDirty;
//! Identification of message to display
extern int nMessageNo;
//! Identification of message to display
extern int nMessageCounter;

//! Timer 2 interrupt service routine
/*!  Timer 2 fires 7 times per second.  After 7 interrupts, the
 *   time variable is incremented and the dirty flag is set to
 *   remind the mainline to update the display.
 *
 *   LED 3 is flashed when this routine is active (the time is
 *   quite short so the LED is hard to see).
 */
void __attribute__((__interrupt__, auto_psv)) _T2Interrupt( void)
{
  _T2IF = 0; // Clear timer 2 interrupt flag

  // All LEDs off
  LATD |= 0x0e;
  nTicker++;
  if ( nTicker == 127 )
    {
        nMessageCounter++;
        if ( nMessageCounter%10 > 6 )
        {
            if (nMessageCounter > 8)
                nMessageCounter = 0;
            nMessageNo = 1;
        }
        else
            nMessageNo = 0;
        lTime ++;
        nTicker = 0;
        nDirty = 1;
        // Blink red LED every second
        _LATD1 = 0;
        // Blink yellow LED every minute
        if ( (lTime % 60) == 0 )
          {
            _LATD3 = 0;
            // Blink green LED every hour
            if ( (lTime % 3600) == 0 )
              _LATD2 = 0;
          }
    }
}
