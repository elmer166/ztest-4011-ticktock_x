/*!  \file T3interrupt.c
 *
 *   \brief Timer 3 interrupt service routine
 *
 *   ISR is used to debounce the buttons
 */
#include <p30Fxxxx.h>

//! Up button
#define PB_UP    _RE0
//! Down button
#define PB_DOWN  _RE1
//! Left button
#define PB_LEFT  _RE2
//! Right button
#define PB_RIGHT _RE3

// Array to remember current button state
int nButtons[4];
// Remember how long button on
int nOnCount[4];
// Remember how long button off
int nOffCount[4];
//! Has a button been pushed?
extern int nAction[4];

//! Timer 3 interrupt service routine
/*! For each button, if it is released, and it has been released for
 * a while, and if it had been pressed for a while, then mark it as
 * a while, and if it had been pressed for a while, then mark it as
 * truly pressed.  Mainline will un-mark it after the expected
 * action has been handled.
 */

void __attribute__((__interrupt__, auto_psv)) _T3Interrupt( void )
{
    // counter to loop through buttons
    int i;

    _T3IF = 0;              // Clear timer 3 interrupt flag

    // Copy each of the button states to an array so we can
    // share debouncing code
    nButtons[0] = PB_LEFT;
    nButtons[1] = PB_RIGHT;
    nButtons[2] = PB_UP;
    nButtons[3] = PB_DOWN;

    // Debouncing code
    for ( i=0; i<4; i++ )                       // Loop through 4 buttons
        if ( !nButtons[i] )                     // If button is depressed
        {
            nOnCount[i]++;                      // Count up time pressed
            nOffCount[i] = 0;                   // and reset un-pressed count
        }
        else                                    // Button released
        {
            nOffCount[i]++;                     // Increment released count
            if ( nOffCount[i] > 5 )             // Released for a while
                if ( nOnCount[i] > 5 )          // Was it actually pressed?
                {
                    nAction[i] = !nAction[i];   // Toggle text display
                    nOnCount[i] = 0;            // Reset pressed count
                }
        }
}
