/*! \file 4011-TickTock.c
 *
 *  \brief  PIC Clock
 *
 * Displays the time and date on the LCD.  Can set time with the
 * up/down and left/right buttons.
 *
 * Two line, 16 character display shows the time as:
 *
 *        14:44:11  UTC
 *       Wed Sep 12 2012
 *
 * Pressing left and right moves a cursor through the time.  Pressing
 * up/down increments or decrements the digit pointed to by the cursor.
 * Following pressing left/right the bottom line is blanked and a carat
 * shown under the selected digit.  After two seconds the date is restored.
 *
 *
 * Author: jjmcd
 *
 * Created on September 11, 2012, 4:13 PM
 */

/******************************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2012 by John J. McDonough, WB8RCR
 *
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 *
 *****************************************************************************/

#include <p30Fxxxx.h>
#include <time.h>
#include <string.h>
// Rather than copy headers into project, use them from the library project
#include "../4011-LCDlib-D.X/lcd.h"
#include "../4011-LCDlib-D.X/delay.h"

// Configuration fuses
_FOSC (XT_PLL16 & PRI);                 // 7.3728 rock * 16 = 118MHz
_FWDT (WDT_OFF);                        // Watchdog timer off
_FBORPOR (PWRT_16 & PBOR_OFF & MCLR_EN);// Brownout off, powerup 16ms
_FGS (GWRP_OFF & CODE_PROT_OFF);        // No code protection

//	Function Prototypes
//! Display a clock on the LCD
int main (void);
//! Display the cursor
void showPos( int );

//    Global variables
//! Current time (actually, a time_t)
long lTime;
//! Used by ISR to stretch out the timer ticks
int nTicker;
//! Does the display ned to be updated?
int nDirty;
//! Has a button been pushed?
int nAction[4];
//! Position of the input cursor
int nPos;
//! Identification of message to display
int nMessageNo;
//! Counter for switching line 2
int nMessageCounter;

//! main - Display a clock on the LCD

/*! {complete description}
 */
int main (void)
{
  int i;                        // Counter
  char szWork[64];              // Result of ctime()
  char szLine1[32],szLine2[32]; // LCD lines

  // Make an LED output (not actually useful it turns out)
  LATD |= 0x000e;
  TRISD &= 0xfff1;

  // Initialize the time and a few other variables
  lTime = 1356018895;       // 2012-12-20
  nTicker = 0;
  nDirty = 0;
  nPos = 4;
  nMessageNo = 0;
  nMessageCounter = 0;
  // Set each button to already handled
  for ( i=0; i<4; i++ )
      nAction[i] = 0;

  // Set up timer 2 for Time
  TMR2 = 0;                 // Clear timer 2
//  PR2 = 16464;              // Timer 2 counter
  PR2 = 29038;
  T2CON = 0x8010;           // Fosc/4, 1:8 prescale, start TMR2
  _T2IE = 1;                // Enable Timer 2 interrupts

  // Set up timer 3 for Buttons
  TMR3 = 0;                 // Clear timer 3
  PR3 = 2000;               // Timer 3 counter to 2000
  T3CON = 0x8030;           // Fosc/4, 1:256 prescale, start TMR3
  _T3IE = 1;                // Enable Timer 3 interrupts

  LCDinit();                // Initialize the LCD
  LCDclear();               // Clear the LCD

  while (1)
    {
      if ( nDirty )                             // Does the LCD need to be
        {                                       // updated?
            // ctime() returns a string like
            // Tue Sep 11 21:12:47 2012
            strcpy(szWork,ctime(&lTime));
            // szWork has an ugly carriage return on the end,
            // get rid of it
            szWork[strlen(szWork)-1] = '\0';
            // Copy the string to buffer for line 2 of the LCD
            strcpy(szLine2,szWork);
            // Whack off the time portion
            szLine2[10] = '\0';
            // Now add back in the year
            strcat(szLine2,&szWork[19]);
            // Put a space at the front of line 1 for better centering
            strcpy(szLine1," ");
            // Add on the time
            strcat(szLine1,&szWork[11]);
            // Whack off the year
            szLine1[10] = '\0';
            // And add a timezone identifier
            strcat(szLine1," UTC");
            // Display line 1
            LCDhome();
            LCDputs(szLine1);
            // Display line 2
            LCDline2();
            if ( nMessageNo == 0 )
                LCDputs(szLine2);
            else
                //LCDputs("      Temp      ");
                LCDputs("  11\337C    52\337F  ");
            // Remember we have updated the display
            nDirty = 0;
        }
      if ( nAction[3] )                     // Has the Down button been pressed?
        {
          nAction[3] = 0;                   // Remember we handled it
          // Now we will subtract the amount of time depending on position
          switch ( nPos )
            {
            case 1:
              lTime -= 36000;               // 10 hours
              break;
            case 2:
              lTime -= 3600;                // 1 hour
              break;
            case 3:
              lTime -= 600;                 // 10 minutes
              break;
            case 4:
              lTime -= 60;                  // One minute
              break;
            case 5:
              lTime -= 10;                  // 10 seconds
              break;
            case 6:
              lTime -= 1;                   // 1 second
            }
          nDirty = 1;                       // Update display
        }
      if ( nAction[2] )                     // Has the Up button been pressed?
        {
          // Same as for Down except we add instead of subtract
          nAction[2] = 0;
          switch ( nPos )
            {
            case 1:
              lTime += 36000;
              break;
            case 2:
              lTime += 3600;
              break;
            case 3:
              lTime += 600;
              break;
            case 4:
              lTime += 60;
              break;
            case 5:
              lTime += 10;
              break;
            case 6:
              lTime += 1;
            }
          nDirty = 1;
        }
      if ( nAction[0] )                     // Has the left button been pressed?
        {
          nAction[0] = 0;                   // Remember we handled it
          nPos--;                           // Move cursor left
          if ( nPos<1 )                     // Wrap to the right
            nPos = 6;
          showPos( nPos );                  // Display the cursor
        }
      if ( nAction[1] )                     // Right button?
        {
          nAction[1] = 0;                   // Same as left button except
          nPos++;                           // we move right and wrap
          if ( nPos>6 )                     // to the left
            nPos = 1;
          showPos( nPos );
        }
    }
}

//! Display the cursor
/*! showPos() blanks the second line of the LCD and displays an
 *  up carat (^) beneath the digit indicated by nPos.  There are
 *  2 colons in the display so we need to account for them
 */
void showPos( int nPos )
{
  // Clear out line 2
  LCDline2();
  LCDputs("                    ");
  // To the right of hours?
  if ( nPos >2 )
    nPos++;
  // To the right of minutes?
  if ( nPos>5 )
    nPos++;
  // Display a carat at the appropriate place
  LCDposition( 0x40 + nPos );
  LCDletter('^');
  // Hang around long enough to see it
  Delay( Delay_1S_Cnt /2 );
  // Remember to restore the display
  nDirty = 1;
}

